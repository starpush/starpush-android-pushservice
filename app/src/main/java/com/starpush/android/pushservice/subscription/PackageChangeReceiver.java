package com.starpush.android.pushservice.subscription;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import static android.content.Intent.EXTRA_DATA_REMOVED;
import static android.content.Intent.EXTRA_REPLACING;

public class PackageChangeReceiver extends BroadcastReceiver {
    private static final String TAG = PackageChangeReceiver.class.getSimpleName();
    private static final String ACTION_PACKAGE_DATA_CLEARED = "android.intent.action.PACKAGE_DATA_CLEARED";
    private static final String ACTION_PACKAGE_FULLY_REMOVED = "android.intent.action.PACKAGE_FULLY_REMOVED";
    private static final String ACTION_PACKAGE_REMOVED = "android.intent.action.PACKAGE_REMOVED";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: Broadcast received. Intent: " + intent);

        String action = intent.getAction();
        if (!(ACTION_PACKAGE_DATA_CLEARED.equals(action) || ACTION_PACKAGE_FULLY_REMOVED.equals(action) || (ACTION_PACKAGE_REMOVED.equals(action) && intent.getBooleanExtra(EXTRA_DATA_REMOVED, false) &&
                !intent.getBooleanExtra(EXTRA_REPLACING, false)))) {
            Log.w(TAG, "Receiver called with unexpected action");
            return;
        }

        Uri data = intent.getData();
        if (data == null) {
            Log.e(TAG, "Package change action carried no data");
            return;
        }

        final String packageName = data.getSchemeSpecificPart();
        Log.i(TAG, "Package removed or data cleared: " + packageName);

        UnsubscriptionWorker.scheduleFullUnsubscription(context, packageName);

    }
}
