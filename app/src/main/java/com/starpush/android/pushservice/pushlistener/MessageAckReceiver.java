package com.starpush.android.pushservice.pushlistener;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.starpush.android.sdk.Constants;

public class MessageAckReceiver extends BroadcastReceiver {
    private static final String TAG = MessageAckReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: Broadcast received. Intent: " + intent.toString());

        String action = intent.getAction();

        if (Constants.ACTION_PUSH_MESSAGE_ACK.equals(action)) {
            String messageToken = intent.getStringExtra(Constants.EXTRA_MESSAGE_TOKEN);
            if (messageToken == null) {
                Log.e(TAG, "Received message ack broadcast without message token");
                return;
            }

            Log.d(TAG, "Got ack request for message with token " + messageToken);
            MessageAckWorker.scheduleAck(context, messageToken);
        } else {
            Log.e(TAG, "Subscription receiver was called with invalid action " + action);
        }
    }
}
