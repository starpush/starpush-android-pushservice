package com.starpush.android.pushservice.subscription;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.starpush.android.pushservice.db.PushDatabase;
import com.starpush.android.pushservice.db.SubscriptionDao;
import com.starpush.android.pushservice.db.entity.SubscriptionEntity;

public class UnsubscriptionWorker extends Worker {
    private static final String TAG = UnsubscriptionWorker.class.getSimpleName();
    private static final String DATA_PACKAGE_NAME = "packageName";
    private static final String DATA_SUBSCRIPTION_TOKEN = "subscriptionToken";
    private static final Constraints CONSTRAINTS = new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build();


    public UnsubscriptionWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        String packageName = getInputData().getString(DATA_PACKAGE_NAME);
        String subscriptionToken = getInputData().getString(DATA_SUBSCRIPTION_TOKEN);
        if ((packageName == null) == (subscriptionToken == null)) {
            Log.e(TAG, "Exactly one of DATA_PACKAGE_NAME or DATA_SUBSCRIPTION_TOKEN should be defined in input data");
            return Result.failure();
        }

        if (packageName != null) {
            // make sure that we do not do more stuff for this package
            WorkManager.getInstance(getApplicationContext()).cancelUniqueWork(packageName);

            PushDatabase db = PushDatabase.getDatabase(getApplicationContext());
            SubscriptionDao dao = db.subscriptionDao();
            SubscriptionEntity subscription = dao.findByPackageName(packageName);

            if (subscription != null) {
                // remove the subscription
                dao.delete(packageName);
                return deleteSubscription(subscription.getSubscriptionToken());
            } else {
                Log.i(TAG, "Package " + packageName + " is not subscribed");
                return Result.success();
            }

        } else {
            if (ApiHelper.unsubscribe(getApplicationContext(), subscriptionToken)) {
                return Result.success();
            } else {
                return Result.retry();
            }
        }
    }

    private Result deleteSubscription(String token) {
        if (ApiHelper.unsubscribe(getApplicationContext(), token)) {
            Log.i(TAG, "Api subscription deletion successful");
            return Result.success();
        } else {
            Log.e(TAG, "Api subscription deletion failed, retrying");
            return Result.retry();
        }
    }

    public static void scheduleApiUnsubscription(Context context, String subscriptionToken) {
        Data data = new Data.Builder().putString(DATA_SUBSCRIPTION_TOKEN, subscriptionToken).build();
        scheduleUnsubscription(context, data);
    }

    public static void scheduleFullUnsubscription(Context context, String packageName) {
        Data data = new Data.Builder().putString(DATA_PACKAGE_NAME, packageName).build();
        scheduleUnsubscription(context, data);
    }

    private static void scheduleUnsubscription(Context context, Data inputData) {
        OneTimeWorkRequest.Builder builder = new OneTimeWorkRequest.Builder(UnsubscriptionWorker.class);
        builder.setConstraints(CONSTRAINTS);
        builder.setInputData(inputData);
        WorkManager.getInstance(context).beginWith(builder.build()).enqueue();
    }
}
