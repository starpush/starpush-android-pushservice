package com.starpush.android.pushservice.db;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.starpush.android.pushservice.db.entity.SubscriptionEntity;

import java.util.List;

@Dao
public interface SubscriptionDao {

    @Query("SELECT * FROM SubscriptionEntity")
    List<SubscriptionEntity> getAll();

    @Query("SELECT * FROM SubscriptionEntity WHERE subscriptionToken LIKE :subscriptionToken LIMIT 1")
    SubscriptionEntity findBySubscriptionToken(String subscriptionToken);

    @Query("SELECT * FROM SubscriptionEntity WHERE packageName LIKE :packageName LIMIT 1")
    SubscriptionEntity findByPackageName(String packageName);

    @Query("SELECT * FROM SubscriptionEntity WHERE subscriptionToken  IS NULL")
    List<SubscriptionEntity> findMissingToken();

    @Insert
    void insert(SubscriptionEntity subscriptionEntity);

    @Update
    void update(SubscriptionEntity subscriptionEntity);

    @Delete
    void delete(SubscriptionEntity subscriptionEntity);

    @Query("DELETE FROM SubscriptionEntity WHERE packageName LIKE :packageName")
    void delete(String packageName);
}
