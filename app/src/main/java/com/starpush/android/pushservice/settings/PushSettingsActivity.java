package com.starpush.android.pushservice.settings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.starpush.android.pushservice.R;
import com.starpush.android.pushservice.db.PushDatabase;
import com.starpush.android.pushservice.db.SubscriptionDao;
import com.starpush.android.pushservice.db.entity.SubscriptionEntity;
import com.starpush.android.pushservice.openapi.ApiClient;
import com.starpush.android.pushservice.openapi.api.VersionApi;
import com.starpush.android.pushservice.openapi.models.Version;
import com.starpush.android.pushservice.pushlistener.PushListenerService;
import com.starpush.android.pushservice.subscription.SubscriptionWorker;
import com.starpush.android.pushservice.subscription.UnsubscriptionWorker;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

public class PushSettingsActivity extends AppCompatActivity {

    private static final String TAG = PushSettingsActivity.class.getSimpleName();

    private TextView mTextView;
    private Button mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_push_settings);

        mTextView = findViewById(R.id.text);
        mTextView.setText(SettingsHelper.getBaseUrl(getApplicationContext()));

        mButton = findViewById(R.id.button);

        Activity activity = this;
        mButton.setOnClickListener(v -> {
            new Thread() {
                @Override
                public void run() {
                    String url = mTextView.getText().toString();
                    Context context = v.getContext();
                    // todo for testing localhost http is ok otherwise use https
                    if (true || URLUtil.isHttpsUrl(url)) {
                        SettingsHelper.setBaseUrl(context, url);
                        ApiClient api = new ApiClient(url);
                        VersionApi versionApi = api.createService(VersionApi.class);
                        try {
                            Response<Version> response = versionApi.versionGet().execute();
                            if (response.isSuccessful()) {
                                Intent intent = new Intent(PushSettingsActivity.this, PushListenerService.class);
                                startService(intent);
                                showToast(activity, "said helo to server");


                                //remove current subscription set cause it is invalid
                                SettingsHelper.setSubscriptionSet(context, null);

                                PushDatabase db = PushDatabase.getDatabase(context);
                                SubscriptionDao dao = db.subscriptionDao();
                                List<SubscriptionEntity> subscriptions = dao.getAll();

                                if (subscriptions != null) {
                                    for (SubscriptionEntity subscription : subscriptions) {
                                        dao.delete(subscription.getPackageName());
                                        SubscriptionWorker.scheduleSubscription(context, subscription.getPackageName());
                                        UnsubscriptionWorker.scheduleApiUnsubscription(context, subscription.getSubscriptionToken());
                                    }
                                }
                                return;
                            }
                            showToast(activity, "Sure right server?");
                        } catch (IOException e) {
                            Log.e(TAG, "Failed to contact push service", e);
                            showToast(activity, "server no say helo");
                            return;
                        }
                    }
                    showToast(activity, "secure Url pliz");
                }
            }.start();
        });
    }

    private void showToast(Activity activity, String text) {
        activity.runOnUiThread(() -> Toast.makeText(activity, text, Toast.LENGTH_SHORT).show());
    }
}