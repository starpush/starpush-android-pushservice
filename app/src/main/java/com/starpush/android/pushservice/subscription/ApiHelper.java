package com.starpush.android.pushservice.subscription;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.starpush.android.pushservice.openapi.ApiClient;
import com.starpush.android.pushservice.openapi.api.MessageApi;
import com.starpush.android.pushservice.openapi.api.SubscriptionApi;
import com.starpush.android.pushservice.openapi.models.Subscription;
import com.starpush.android.pushservice.openapi.models.SubscriptionSet;
import com.starpush.android.pushservice.pushlistener.PushListenerService;
import com.starpush.android.pushservice.settings.PushSettingsActivity;
import com.starpush.android.pushservice.settings.SettingsHelper;

import java.io.IOException;

import retrofit2.Response;

public class ApiHelper {
    private static final String TAG = SubscriptionWorker.class.getSimpleName();

    /**
     * @param context     Cause android
     * @param packageName The name of the package for which we want to create a subscription
     * @return true indicating success, false otherwise
     */
    public static Subscription subscribe(Context context, String packageName) {
        Log.d(TAG, "Asking API for subscription for package " + packageName);

        String uri = SettingsHelper.getBaseUrl(context);
        String subscriptionSetToken = SettingsHelper.getSubscriptionSet(context);

        SubscriptionSet subscriptionSet = new SubscriptionSet();
        subscriptionSet.setSubscriptionSetToken(subscriptionSetToken);

        ApiClient api = new ApiClient(uri);
        SubscriptionApi subscriptionApi = api.createService(SubscriptionApi.class);
        Response<Subscription> response;
        try {
            response = subscriptionApi.subscribePost(subscriptionSet).execute();
        } catch (IOException e) {
            Log.e(TAG, "Caught error when attempting to subscribe", e);
            return null;
        }

        Subscription subscription = response.body();
        if (response.isSuccessful() && subscription != null) {
            // if this is the first subscription we need to store the corresponding set
            if (subscriptionSetToken == null) {
                SettingsHelper.setSubscriptionSet(context, subscription.getSubscriptionSet().getSubscriptionSetToken());
            }
            return subscription;
        } else if(response.code() == 400){
            // we have a bad subscription set
            // make sure that the listener service runs. it will fix that
            // todo run re-subscription
        }

        // TODO handle special failures / warn user about overutilization of server
        Log.e(TAG, "Failed to create subscription. http code: " + response.code());
        return null;
    }

    /**
     * @param context           Cause android
     * @param subscriptionToken The token of the subscription to delete
     * @return true indicating success, false otherwise
     */
    public static boolean unsubscribe(Context context, String subscriptionToken) {
        String uri = SettingsHelper.getBaseUrl(context);
        ApiClient api = new ApiClient(uri);

        SubscriptionApi subscriptionApi = api.createService(SubscriptionApi.class);

        try {
            Response<Void> response = subscriptionApi.subscriptionTokenDelete(subscriptionToken).execute();
            // 404 indicates that the subscription is not known
            return response.isSuccessful() || response.code() == 404;
        } catch (IOException e) {
            // TODO handle error ?
            return false;
        }
    }

    public static boolean ackMessage(Context context, String messageToken) {
        String uri = SettingsHelper.getBaseUrl(context);
        ApiClient api = new ApiClient(uri);

        MessageApi messageApi = api.createService(MessageApi.class);

        try {
            Response<Void> response = messageApi.messageTokenDelete(messageToken).execute();
            // 404 indicates that the message is not known or already acked
            return response.isSuccessful() || response.code() == 404;
        } catch (IOException e) {
            // TODO handle error ?
            return false;
        }
    }

}
