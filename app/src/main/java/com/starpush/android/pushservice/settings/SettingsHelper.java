package com.starpush.android.pushservice.settings;

import android.content.Context;

public class SettingsHelper {
    private static final String PREFERENCE_BASE_URL = "base_url";
    private static final String PREFERENCE_BASE_URL_DEFAULT = null;

    private static final String PREFERENCE_SUBSCRIPTION_SET = "subscription_set";
    private static final String PREFERENCE_SUBSCRIPTION_SET_DEFAULT = null;

    private static final String PREFERENCES = "com.starpush.android.pushservice.prefs";

    public static String getBaseUrl(Context context) {
        return getStringSetting(context, PREFERENCE_BASE_URL, PREFERENCE_BASE_URL_DEFAULT);
    }

    public static void setBaseUrl(Context context, String baseURL) {
        setStringSetting(context, PREFERENCE_BASE_URL, baseURL);
    }

    public static String getSubscriptionSet(Context context) {
        return getStringSetting(context, PREFERENCE_SUBSCRIPTION_SET, PREFERENCE_SUBSCRIPTION_SET_DEFAULT);
    }

    public static void setSubscriptionSet(Context context, String subscriptionSet) {
        setStringSetting(context, PREFERENCE_SUBSCRIPTION_SET, subscriptionSet);
    }

    private static String getStringSetting(Context context, String key, String defaultValue) {
        return context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE).getString(key, defaultValue);
    }

    private static void setStringSetting(Context context, String key, String value) {
        context.getSharedPreferences(PREFERENCES, Context.MODE_PRIVATE).edit().putString(key, value).apply();
    }

}
