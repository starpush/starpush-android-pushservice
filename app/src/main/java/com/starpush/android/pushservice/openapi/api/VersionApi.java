package com.starpush.android.pushservice.openapi.api;

import com.starpush.android.pushservice.openapi.CollectionFormats.*;

import retrofit2.Call;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MultipartBody;

import com.starpush.android.pushservice.openapi.models.Version;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface VersionApi {
  /**
   * Return the server api version.
   * 
   * @return Call&lt;Version&gt;
   */
  @GET("version")
  Call<Version> versionGet();
    

}
