package com.starpush.android.pushservice.openapi.api;

import com.starpush.android.pushservice.openapi.CollectionFormats.*;

import retrofit2.Call;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MultipartBody;

import com.starpush.android.pushservice.openapi.models.PushMessage;
import com.starpush.android.pushservice.openapi.models.Subscription;
import com.starpush.android.pushservice.openapi.models.SubscriptionSet;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface SubscriptionApi {
  /**
   * create a new push subscription
   * 
   * @param subscriptionSet  (optional)
   * @return Call&lt;Subscription&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("subscribe")
  Call<Subscription> subscribePost(
    @retrofit2.http.Body SubscriptionSet subscriptionSet
  );

  /**
   * Delete the subscription set and all related subscriptions.
   * 
   * @param token The token for the subscription set. (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("subscription-set/{token}")
  Call<Void> subscriptionSetTokenDelete(
    @retrofit2.http.Path("token") String token
  );

  /**
   * Request messages belonging to this subscription set.
   * 
   * @param token The token for the subscription set. (required)
   * @return Call&lt;List&lt;PushMessage&gt;&gt;
   */
  @GET("subscription-set/{token}")
  Call<List<PushMessage>> subscriptionSetTokenGet(
    @retrofit2.http.Path("token") String token
  );

  /**
   * Delete the subscription.
   * 
   * @param token The token for the subscription. (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("subscription/{token}")
  Call<Void> subscriptionTokenDelete(
    @retrofit2.http.Path("token") String token
  );

  /**
   * Request messages belonging to this subscription.
   * 
   * @param token The token for the subscription. (required)
   * @return Call&lt;List&lt;PushMessage&gt;&gt;
   */
  @GET("subscription/{token}")
  Call<List<PushMessage>> subscriptionTokenGet(
    @retrofit2.http.Path("token") String token
  );

}
