/*
 * Copyright (C) 2018 microG Project Team
 * Copyright (C) 2020 Marcus Hoffmann
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.starpush.android.pushservice.subscription;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.starpush.android.sdk.Constants;

public class SubscriptionReceiver extends BroadcastReceiver {
    private static final String TAG = SubscriptionReceiver.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive: Broadcast received. Intent: " + intent.toString());

        // find the name of the package that sent this request
        // todo is there a way to get the actual requesting package? in a service
        String packageName = intent.getStringExtra(Constants.EXTRA_PACKAGE_NAME);

        String action = intent.getAction();
        if (Constants.ACTION_PUSH_REGISTER.equals(action)) {
            SubscriptionWorker.scheduleSubscription(context, packageName);
        } else if (Constants.ACTION_PUSH_UNREGISTER.equals(action)) {
            UnsubscriptionWorker.scheduleFullUnsubscription(context, packageName);
        } else if (null == action) {
            Log.e(TAG, "Subscription receiver was called without an action specified");
        } else {
            Log.e(TAG, "Subscription receiver was called with invalid action " + action);
        }
    }

}
