package com.starpush.android.pushservice.pushlistener;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.starpush.android.pushservice.subscription.ApiHelper;


// performs the token requests for all the subscriptions in the database that do not have a token
public class MessageAckWorker extends Worker {
    private static final String TAG = MessageAckWorker.class.getSimpleName();
    private static final Constraints CONSTRAINTS = new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build();
    private static final String DATA_MESSAGE_TOKEN = "messageToken";

    public MessageAckWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        String messageToken = getInputData().getString(DATA_MESSAGE_TOKEN);
        if (messageToken == null) {
            Log.e(TAG, "Work parameter DATA_MESSAGE_TOKEN is missing");
            return Result.failure();
        }

        boolean success = ApiHelper.ackMessage(getApplicationContext(), messageToken);

        if (success) {
            return Result.success();
        } else {
            return Result.retry();
        }
    }


    public static void scheduleAck(Context context, String messageToken) {
        Data data = new Data.Builder().putString(DATA_MESSAGE_TOKEN, messageToken).build();
        OneTimeWorkRequest.Builder builder = new OneTimeWorkRequest.Builder(MessageAckWorker.class);
        builder.setConstraints(CONSTRAINTS);
        builder.setInputData(data);
        WorkManager.getInstance(context).beginUniqueWork(messageToken, ExistingWorkPolicy.KEEP, builder.build()).enqueue();
        Log.d(TAG, "Scheduled message ack for message with token" + messageToken);
    }
}
