package com.starpush.android.pushservice.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.starpush.android.pushservice.db.entity.SubscriptionEntity;

@Database(entities = {SubscriptionEntity.class}, version = 1, exportSchema = false)
public abstract class PushServiceDatabase extends RoomDatabase {
    public abstract SubscriptionDao subscriptionDao();
}
