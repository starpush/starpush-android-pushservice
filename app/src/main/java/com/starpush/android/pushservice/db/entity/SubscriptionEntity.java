package com.starpush.android.pushservice.db.entity;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;


@Entity(indices = {@Index(value = "pushToken", unique = true), @Index(value = "subscriptionToken", unique = true)})
public class SubscriptionEntity {

    @PrimaryKey
    @NonNull
    private String packageName;

    @NonNull
    private String subscriptionToken;

    @NonNull
    private String pushToken;

    public SubscriptionEntity(@NonNull String packageName, @NonNull String subscriptionToken, @NonNull String pushToken) {
        this.packageName = packageName;
        this.subscriptionToken = subscriptionToken;
        this.pushToken = pushToken;
    }

    @NonNull
    public String getSubscriptionToken() {
        return subscriptionToken;
    }

    public void setSubscriptionToken(@NonNull String subscriptionToken) {
        this.subscriptionToken = subscriptionToken;
    }

    @NonNull
    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(@NonNull String packageName) {
        this.packageName = packageName;
    }

    @NonNull
    public String getPushToken() {
        return pushToken;
    }

    public void setPushToken(@NonNull String pushToken) {
        this.pushToken = pushToken;
    }
}
