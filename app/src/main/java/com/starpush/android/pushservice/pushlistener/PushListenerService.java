package com.starpush.android.pushservice.pushlistener;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.starpush.android.pushservice.R;
import com.starpush.android.pushservice.settings.PushSettingsActivity;

public class PushListenerService extends Service {
    public static final String CHANNEL_ID = "ForegroundServiceChannel";
    private static final String TAG = PushListenerService.class.getSimpleName();

    private PushListenerThread listenerThread = null;


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (listenerThread == null || !listenerThread.isAlive()) {
            Log.i(TAG, "Starting push service");
            setupService();
        } else {
            Log.d(TAG, "Push service is already running");
        }

        return START_STICKY;
    }

    private void setupService() {

        listenerThread = new PushListenerThread(getApplicationContext());
        listenerThread.start();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel serviceChannel = new NotificationChannel(
                    CHANNEL_ID,
                    "Foreground Service Channel",
                    NotificationManager.IMPORTANCE_LOW
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            if (manager != null) {
                manager.createNotificationChannel(serviceChannel);
            }
        }

        Intent notificationIntent = new Intent(this, PushSettingsActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                0, notificationIntent, 0);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Push Listener")
                .setContentText("You are seeing this because the push service is not integrated into your ROM")
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentIntent(pendingIntent)
                .build();

        startForeground(1, notification);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

}
