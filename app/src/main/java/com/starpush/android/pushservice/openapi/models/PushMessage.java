/*
 * Starpush API
 * HTTP API for the starpush system.
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.starpush.android.pushservice.openapi.models;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * The message to transmit
 */
@ApiModel(description = "The message to transmit")
@javax.annotation.Generated(value = "org.openapitools.codegen.languages.JavaClientCodegen", date = "2020-07-26T23:42:40.167+02:00[Europe/Zurich]")
public class PushMessage {
  public static final String SERIALIZED_NAME_MESSAGE_TOKEN = "message-token";
  @SerializedName(SERIALIZED_NAME_MESSAGE_TOKEN)
  private String messageToken;

  public static final String SERIALIZED_NAME_PAYLOAD = "payload";
  @SerializedName(SERIALIZED_NAME_PAYLOAD)
  private String payload;

  public static final String SERIALIZED_NAME_SUBSCRIPTION = "subscription";
  @SerializedName(SERIALIZED_NAME_SUBSCRIPTION)
  private String subscription;

  public PushMessage messageToken(String messageToken) {
    this.messageToken = messageToken;
    return this;
  }

   /**
   * Identifier for a resource
   * @return messageToken
  **/
  @ApiModelProperty(example = "WW91IGZvdW5kIG1lISEhIDotTw", required = true, value = "Identifier for a resource")
  public String getMessageToken() {
    return messageToken;
  }

  public void setMessageToken(String messageToken) {
    this.messageToken = messageToken;
  }

  public PushMessage payload(String payload) {
    this.payload = payload;
    return this;
  }

   /**
   * the payload to transmit
   * @return payload
  **/
  @ApiModelProperty(example = "Hello world!", required = true, value = "the payload to transmit")
  public String getPayload() {
    return payload;
  }

  public void setPayload(String payload) {
    this.payload = payload;
  }

  public PushMessage subscription(String subscription) {
    this.subscription = subscription;
    return this;
  }

   /**
   * Identifier for a resource
   * @return subscription
  **/
  @ApiModelProperty(example = "WW91IGZvdW5kIG1lISEhIDotTw", required = true, value = "Identifier for a resource")
  public String getSubscription() {
    return subscription;
  }

  public void setSubscription(String subscription) {
    this.subscription = subscription;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    PushMessage pushMessage = (PushMessage) o;
    return Objects.equals(this.messageToken, pushMessage.messageToken) &&
        Objects.equals(this.payload, pushMessage.payload) &&
        Objects.equals(this.subscription, pushMessage.subscription);
  }

  @Override
  public int hashCode() {
    return Objects.hash(messageToken, payload, subscription);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class PushMessage {\n");
    
    sb.append("    messageToken: ").append(toIndentedString(messageToken)).append("\n");
    sb.append("    payload: ").append(toIndentedString(payload)).append("\n");
    sb.append("    subscription: ").append(toIndentedString(subscription)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

