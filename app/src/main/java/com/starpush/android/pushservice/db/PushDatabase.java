package com.starpush.android.pushservice.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.starpush.android.pushservice.db.entity.SubscriptionEntity;

@Database(entities = {SubscriptionEntity.class}, version = 1, exportSchema = false)
public abstract class PushDatabase extends RoomDatabase {
    public abstract SubscriptionDao subscriptionDao();
    private static volatile PushDatabase INSTANCE;

    public static PushDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (PushDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            PushDatabase.class, "pushclient_database")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
