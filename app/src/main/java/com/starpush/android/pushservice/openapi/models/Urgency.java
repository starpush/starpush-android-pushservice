/*
 * Starpush API
 * HTTP API for the starpush system.
 *
 * OpenAPI spec version: 0.0.1
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


package com.starpush.android.pushservice.openapi.models;

import java.util.Objects;
import java.util.Arrays;
import io.swagger.annotations.ApiModel;
import com.google.gson.annotations.SerializedName;

import java.io.IOException;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * Urgency of a push message
 */
@JsonAdapter(Urgency.Adapter.class)
public enum Urgency {
  
  VERY_LOW("very-low"),
  
  LOW("low"),
  
  NORMAL("normal"),
  
  HIGH("high");

  private String value;

  Urgency(String value) {
    this.value = value;
  }

  public String getValue() {
    return value;
  }

  @Override
  public String toString() {
    return String.valueOf(value);
  }

  public static Urgency fromValue(String text) {
    for (Urgency b : Urgency.values()) {
      if (String.valueOf(b.value).equals(text)) {
        return b;
      }
    }
    throw new IllegalArgumentException("Unexpected value '" + text + "'");
  }

  public static class Adapter extends TypeAdapter<Urgency> {
    @Override
    public void write(final JsonWriter jsonWriter, final Urgency enumeration) throws IOException {
      jsonWriter.value(enumeration.getValue());
    }

    @Override
    public Urgency read(final JsonReader jsonReader) throws IOException {
      String value = jsonReader.nextString();
      return Urgency.fromValue(String.valueOf(value));
    }
  }
}

