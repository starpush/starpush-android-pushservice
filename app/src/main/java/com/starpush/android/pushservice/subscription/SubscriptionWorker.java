package com.starpush.android.pushservice.subscription;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Constraints;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.NetworkType;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.starpush.android.pushservice.db.PushDatabase;
import com.starpush.android.pushservice.db.SubscriptionDao;
import com.starpush.android.pushservice.db.entity.SubscriptionEntity;
import com.starpush.android.pushservice.openapi.models.Subscription;
import com.starpush.android.pushservice.pushlistener.PushListenerService;
import com.starpush.android.sdk.Constants;


// performs the token requests for all the subscriptions in the database that do not have a token
public class SubscriptionWorker extends Worker {
    private static final String TAG = SubscriptionWorker.class.getSimpleName();
    private static final Constraints CONSTRAINTS = new Constraints.Builder().setRequiredNetworkType(NetworkType.CONNECTED).build();
    private static final String DATA_PACKAGE_NAME = "packageName";

    public SubscriptionWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        String packageName = getInputData().getString(DATA_PACKAGE_NAME);
        if (packageName == null) {
            Log.e(TAG, "Work parameter DATA_PACKAGE_NAME is missing");
            return Result.failure();
        }

        PushDatabase db = PushDatabase.getDatabase(getApplicationContext());
        SubscriptionDao dao = db.subscriptionDao();
        SubscriptionEntity subscriptionEntity = dao.findByPackageName(packageName);

        if (subscriptionEntity != null) {
            String token = subscriptionEntity.getPushToken();
            Log.i(TAG, String.format("Package '%s' is already subscribed", packageName));
            sendToken(packageName, token);
            return Result.success();
        }

        Subscription subscription = ApiHelper.subscribe(getApplicationContext(), packageName);

        if (subscription == null) {
            Log.w(TAG, "Failed create new subscription for package " + packageName);
            return Result.retry();
        }

        // store the new subscription
        subscriptionEntity = new SubscriptionEntity(packageName, subscription.getSubscriptionToken(), subscription.getPushToken());
        dao.insert(subscriptionEntity);

        // make sure we are listening to the new subscription
        // start the push listener service
        Intent intent = new Intent(getApplicationContext(), PushListenerService.class);
        getApplicationContext().startService(intent);

        //inform the requesting app about token
        sendToken(packageName, subscription.getPushToken());

        return Result.success();
    }


    public static void scheduleSubscription(Context context, String packageName) {
        Data data = new Data.Builder().putString(DATA_PACKAGE_NAME, packageName).build();
        OneTimeWorkRequest.Builder builder = new OneTimeWorkRequest.Builder(SubscriptionWorker.class);
        builder.setConstraints(CONSTRAINTS);
        builder.setInputData(data);
        WorkManager.getInstance(context).beginUniqueWork(packageName, ExistingWorkPolicy.KEEP, builder.build()).enqueue();
        Log.d(TAG, "Scheduled resubscription for package " + packageName);
    }


    private void sendToken(String targetPackage, String token) {
        Intent intent = new Intent();
        intent.setAction(Constants.ACTION_PUSH_RECEIVE);
        intent.setPackage(targetPackage);
        intent.putExtra(Constants.EXTRA_PUSH_TOKEN, token);
        getApplicationContext().startService(intent);
        Log.i(TAG, "Sent push token to package " + targetPackage);
    }
}
