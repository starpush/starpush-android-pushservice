package com.starpush.android.pushservice.pushlistener;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.starpush.android.pushservice.db.PushDatabase;
import com.starpush.android.pushservice.db.SubscriptionDao;
import com.starpush.android.pushservice.db.entity.SubscriptionEntity;
import com.starpush.android.pushservice.openapi.ApiClient;
import com.starpush.android.pushservice.openapi.api.SubscriptionApi;
import com.starpush.android.pushservice.openapi.models.PushMessage;
import com.starpush.android.pushservice.settings.SettingsHelper;
import com.starpush.android.pushservice.subscription.SubscriptionWorker;
import com.starpush.android.pushservice.subscription.UnsubscriptionWorker;
import com.starpush.android.sdk.Constants;

import java.io.IOException;
import java.util.List;

import retrofit2.Response;

public class PushListenerThread extends Thread {
    private static final String TAG = PushListenerThread.class.getSimpleName();
    private final Context context;
    private final SubscriptionDao dao;
    private final ApiClient client;
    private final SubscriptionApi subscriptionApi;

    public PushListenerThread(Context context) {
        this.context = context;
        PushDatabase db = PushDatabase.getDatabase(context);
        dao = db.subscriptionDao();

        String uri = SettingsHelper.getBaseUrl(context);
        client = new ApiClient(uri);
        subscriptionApi = client.createService(SubscriptionApi.class);
    }

    @Override
    public void run() {
        while (true) {
            if (Thread.currentThread().isInterrupted()) {
                Log.e(TAG, "was interrupted");
                return;
            }

            String subscriptionSet = SettingsHelper.getSubscriptionSet(context);
            if (subscriptionSet != null) {
                Log.d(TAG, "Checking for messages");
                try {
                    Response<List<PushMessage>> response = subscriptionApi.subscriptionSetTokenGet(subscriptionSet).execute();
                    if (response.isSuccessful()) {
                        List<PushMessage> messages = response.body();
                        if (messages != null) {
                            distributeMessages(messages);
                        }
                    } else if (response.code() == 404) {
                        handleReSubscription();
                    }
                } catch (RuntimeException | IOException e) {
                    Log.e(TAG, "Caught an error when listening for new messages", e);
                }
            } else {
                Log.d(TAG, "Skipping run because token is null");
            }

            try {
                // check for messages every 10 seconds
                Thread.sleep(10_000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void distributeMessages(@NonNull List<PushMessage> messages) {
        for (PushMessage message : messages) {
            String subscriptionToken = message.getSubscription();
            SubscriptionEntity subscription = dao.findBySubscriptionToken(subscriptionToken);
            if (subscription == null) {
                // this means we are receiving messages for a subscription that we don't know about
                // we want to delete the subscription
                UnsubscriptionWorker.scheduleApiUnsubscription(context, subscriptionToken);
            } else {
                sendMessage(subscription.getPackageName(), message);
            }
        }
    }

    /**
     * delete all subscriptions and schedule new subscription
     * TODO make better. currently we lose a subscription if we first delete and then before schedule re-subscription we crash/are killed
     */
    private void handleReSubscription() {
        Log.w(TAG, "Running re-subscription");
        //remove current subscription set cause it is invalid
        SettingsHelper.setSubscriptionSet(context, null);

        List<SubscriptionEntity> subscriptions = dao.getAll();

        if (subscriptions != null) {
            Log.i(TAG, "Resubscription for " + subscriptions.size() + " packages.");
            for (SubscriptionEntity subscription : subscriptions) {
                dao.delete(subscription.getPackageName());
                SubscriptionWorker.scheduleSubscription(context, subscription.getPackageName());
                UnsubscriptionWorker.scheduleApiUnsubscription(context, subscription.getSubscriptionToken());
            }
        } else {
            Log.e(TAG, "Subscriptions is null");
        }
    }

    private void sendMessage(String packageName, PushMessage message) {
        Intent intent = new Intent();
        intent.setPackage(packageName);
        intent.putExtra(Constants.EXTRA_MESSAGE_PAYLOAD, message.getPayload());
        intent.putExtra(Constants.EXTRA_MESSAGE_TOKEN, message.getMessageToken());
        intent.setAction(Constants.ACTION_PUSH_RECEIVE);
        context.startService(intent);

        Log.d(TAG, "sent message with token " + message.getMessageToken() + " to package " + packageName);
    }
}