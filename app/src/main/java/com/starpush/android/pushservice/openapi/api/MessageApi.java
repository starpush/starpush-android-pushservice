package com.starpush.android.pushservice.openapi.api;

import com.starpush.android.pushservice.openapi.CollectionFormats.*;

import retrofit2.Call;
import retrofit2.http.*;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import okhttp3.MultipartBody;

import com.starpush.android.pushservice.openapi.models.PushRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface MessageApi {
  /**
   * Delete a message that was previously sent
   * 
   * @param token The token for the push message. (required)
   * @return Call&lt;Void&gt;
   */
  @DELETE("message/{token}")
  Call<Void> messageTokenDelete(
    @retrofit2.http.Path("token") String token
  );

  /**
   * Request push message delivery
   * 
   * @param token The token to access the push resource. (required)
   * @param pushRequest the message (required)
   * @return Call&lt;Void&gt;
   */
  @Headers({
    "Content-Type:application/json"
  })
  @POST("push/{token}")
  Call<Void> pushTokenPost(
    @retrofit2.http.Path("token") String token, @retrofit2.http.Body PushRequest pushRequest
  );

}
